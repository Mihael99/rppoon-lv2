﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1i2Zad
{
    class Program
    {
        static void Print(IList<int> RollingResults)
        {
            foreach (int result in RollingResults) {
                Console.WriteLine(result);
                    }
        }
        static void Main(string[] args)
        {
            //Zadatak 1.
            //Random randomize = new Random();
            //Die[] die = new Die[20];
            //for (int i = 0; i < 20; i++)
            //{
            //    int randomSides = randomize.Next(3, 7);
            //    die[i] = new Die(randomSides);
            //}

            //DiceRoller rollerOne = new DiceRoller();
            //for (int i = 0; i < 20; i++)
            //{
            //    rollerOne.InsertDie(die[i]);
            //}

            //rollerOne.RollAllDice();
            //Print(rollerOne.GetRollingResults());

            //Zadatak 2.
            //Random randomize = new Random();
            //Die[] die = new Die[20];
            //for (int i = 0; i < 20; i++)
            //{
            //    int randomSides = randomize.Next(3, 7);
            //    die[i] = new Die(randomSides, randomize);
            //}

            //DiceRoller rollerOne = new DiceRoller();
            //for (int i = 0; i < 20; i++)
            //{
            //    rollerOne.InsertDie(die[i]);
            //}

            //rollerOne.RollAllDice();
            //Print(rollerOne.GetRollingResults());

            //Zadatak 3.
            //Random randomize = new Random();
            //Die[] die = new Die[20];
            //for (int i = 0; i < 20; i++)
            //{
            //    int randomSides = randomize.Next(3, 7);
            //    die[i] = new Die(randomSides);
            //}

            //DiceRoller rollerOne = new DiceRoller();
            //for (int i = 0; i < 20; i++)
            //{
            //    rollerOne.InsertDie(die[i]);
            //}

            //rollerOne.RollAllDice();
            //Print(rollerOne.GetRollingResults());

            //Zadatak 4.
            Random randomize = new Random();
            Die[] die = new Die[20];
            for (int i = 0; i < 20; i++)
            {
                int randomSides = randomize.Next(3, 7);
                die[i] = new Die(randomSides);
            }

            DiceRoller rollerOne = new DiceRoller();
            for (int i = 0; i < 20; i++)
            {
                rollerOne.InsertDie(die[i]);
            }

            rollerOne.RollAllDice();
            Print(rollerOne.GetRollingResults());
            rollerOne.ChooseLogger();
            Console.WriteLine("Logger:\n");
            rollerOne.LogRollingResults();

        }
    }
}
