﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1i2Zad
{
    class Die
    {
        //zadatak 1
        //private int numberOfSides;
        //private Random randomGenerator;
        //public Die(int numberOfSides)
        //{
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = new Random();
        //}
        //public int Roll()
        //{
        //    int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
        //    return rolledNumber;
        //}

        //zadatak 2
        //private int numberOfSides;
        //private Random randomGenerator;
        //public Die(int numberOfSides, Random rand)
        //{
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = rand;
        //}
        //public int Roll()
        //{
        //    int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
        //    return rolledNumber;
        //}

        //Zadatak 3
        private int numberOfSides;
        private RandomGenerator randomGenerator;
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = RandomGenerator.GetInstance();
        }
        public int Roll()
        {
            int rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }

        //Zadatak 7.
        public int getNumberOfSides()
        {
            return numberOfSides;
        }
    }
}
