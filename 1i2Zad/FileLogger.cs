﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1i2Zad
{
    class FileLogger: ILogger
    {
        private string filePath;
        public FileLogger(string filePath)
        {
            this.filePath = filePath;
        }
        //Zadatak 4.
        public void Log(string message)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath))
            {
                writer.WriteLine(message);
            }
        }

        //Zadatak 5.
        //public void Log(ILogable data)
        //{
        //    using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath))
        //    {
        //        writer.WriteLine(data.GetStringRepresentation());
        //    }
        //}
    }
}
