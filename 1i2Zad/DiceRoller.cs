﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1i2Zad
{
    class DiceRoller/*: ILogable*/
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        private ILogger logger;

        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RollAllDice()
        {//clear results of previous rolling
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
        //View of the results
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(this.resultForEachRoll);
        }
        public int DiceCount
        {
            get { return dice.Count; }
        }

        //Zadatak 4
        public void LogRollingResults()
        {
            StringBuilder sb = new StringBuilder();
            foreach (int result in this.resultForEachRoll)
            {
                sb.Append(result);
                sb.Append("\n");
            }
            logger.Log(sb.ToString());
        }
        public void ChooseLogger()
        {
            Console.WriteLine("Where do you want to log your rolls?\n 1. Console Logger \n 2. File Logger");
            int numberOfChoice = Convert.ToInt32(Console.ReadLine());
            if (numberOfChoice == 1)
            {
                logger = new ConsoleLogger();
            }
            else if (numberOfChoice == 2)
            {
                logger = new FileLogger("E:\\Downloads\\log.txt");
            }

            }

            //Zadatak 5.
            //public string GetStringRepresentation()
            //{
            //}
        }
    
}
