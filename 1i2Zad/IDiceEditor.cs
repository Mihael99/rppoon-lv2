﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1i2Zad
{
    interface IDiceEditor
    {
        //Zadatak 6.
        void InsertDie(Die die);
        void RemoveAllDice();
    }
}
