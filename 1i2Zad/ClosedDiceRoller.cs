﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1i2Zad
{
    //6 zadatak
    class ClosedDiceRoller : IDiceRoller
    {
        private List<Die> dice;
        private List<int> resultforEachRoll;

        public ClosedDiceRoller(int diceCount, int numberOfSides)
        {
            this.dice = new List<Die>();
            for (int i = 0; i < diceCount; i++)
            {
                this.dice.Add(new Die(numberOfSides));
            }
            this.resultforEachRoll = new List<int>();
        }
        public void RollAllDice()
        {
            this.resultforEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultforEachRoll.Add(die.Roll());
            }
        }
    }
}
