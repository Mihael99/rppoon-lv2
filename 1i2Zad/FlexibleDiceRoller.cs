﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1i2Zad
{
    //6 zadatak.
    class FlexibleDiceRoller: IDiceEditor, IDiceRoller
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;

        public FlexibleDiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }

        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RemoveAllDice()
        {
            this.dice.Clear();
            this.resultForEachRoll.Clear();
        }
        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach(Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }

        //Zadatak 7.
        public void RemoveDiceByNumberOfSides(int numberOfSides)
        {   
            for (int i = 0; i < dice.Count(); i++)
            {
                if(dice.ElementAt(i).getNumberOfSides() == numberOfSides)
                {
                    dice.RemoveAt(i);
                }
            }
        }
    }
}
